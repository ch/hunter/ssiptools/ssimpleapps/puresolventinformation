#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test methods for information. Checks to ensure files are present.

@author: mark
"""


import logging
import sys
import pathlib
import unittest
import puresolventinformation.information as pureinf

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

class InformationTestCase(unittest.TestCase):
    """Test Case to check information consistency."""

    def test_single_solute_file(self):
        """Assert if single solute file exists.

        Returns
        -------
        None.

        """
        self.assertTrue(pathlib.Path(pureinf.SINGLE_SSIP_SOLUTE_FILE).is_file())
    def test_get_name_inchikey_mapping(self):
        """Assert expected number of entries are found.

        Returns
        -------
        None.

        """
        inchikey_mapping = pureinf.get_name_inchikey_mapping()
        self.assertEqual(261, len(inchikey_mapping))
        self.assertEqual("BTANRVKWQNVYAZ-BYPYZUCNSA-N", inchikey_mapping["2-butanol"])
    def test_get_ssip_file_names(self):
        """Assert expected number of SSIP files found.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len([val for val in pureinf.get_ssip_file_names()]))
        for filename in pureinf.get_ssip_file_names():
            with self.subTest(filename=filename):
                self.assertTrue(filename.is_file())
    def test_get_polarised_ssip_file_names(self):
        """Assert expected number of SSIP files found.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len([val for val in pureinf.get_polarised_ssip_filenames()]))
        for filename in pureinf.get_polarised_ssip_filenames():
            with self.subTest(filename=filename):
                self.assertTrue(filename.is_file())
    def test_get_unpolarised_ssip_file_names(self):
        """Assert expected number of SSIP files found.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len([val for val in pureinf.get_unpolarised_ssip_filenames()]))
        for filename in pureinf.get_unpolarised_ssip_filenames():
            with self.subTest(filename=filename):
                self.assertTrue(filename.is_file())
    def test_get_ssip_file_dict(self):
        """Assert expected number of files in dict.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_ssip_file_dict()))
    def test_get_polarised_ssip_file_dict(self):
        """Assert expected number of files in dict.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_polarised_ssip_file_dict()))
    def test_get_unpolarised_ssip_file_dict(self):
        """Assert expected number of files in dict.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_unpolarised_ssip_file_dict()))
    def test_get_polynomial_filenames(self):
        """Assert expected number of filenames and they are files.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_polynomial_filenames()))
        for filename in pureinf.get_polynomial_filenames():
            with self.subTest(filename=filename):
                self.assertTrue(pathlib.Path(filename).is_file())
    def test_get_polarised_polynomial_filenames(self):
        """Assert expected number of filenames and they are files.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_polarised_polynomial_filenames()))
        for filename in pureinf.get_polarised_polynomial_filenames():
            with self.subTest(filename=filename):
                self.assertTrue(pathlib.Path(filename).is_file())
    def test_get_unpolarised_polynomial_filenames(self):
        """Assert expected number of filenames and they are files.

        Returns
        -------
        None.

        """
        self.assertEqual(261, len(pureinf.get_unpolarised_polynomial_filenames()))
        for filename in pureinf.get_unpolarised_polynomial_filenames():
            with self.subTest(filename=filename):
                self.assertTrue(pathlib.Path(filename).is_file())
    def test_get_similarity_domain_information(self):
        """Assert similarity domain information is returned.

        Returns
        -------
        None.

        """
        self.assertTrue(pureinf.get_similarity_domain_information() is not None)

def create_test_suite():
    """Create a test suite with all the tests from the package.

    Returns
    -------
    suite : test suite
        test suite with all tests of package loaded.

    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    LOGGER.debug("Adding %s", InformationTestCase)
    suite.addTests(loader.loadTestsFromTestCase(InformationTestCase))
    return suite


def run_tests():
    """Run test suite. Exits if there is a failure.

    Returns
    -------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()