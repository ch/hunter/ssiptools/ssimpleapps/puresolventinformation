# -*- coding: utf-8 -*-
#    puresolventinformation contains default information on the set of pure solvents for FGIP analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    puresolventinformation is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Resource location information for pure solvents and similarity domain
information and normalisation coefficents.

Contains:

- Paths to single SSIP Solute file.
- Paths to free energy polynomials for pure solvents.
- Paths to SSIP files.
- Solvent similarity domain information, including normalisation coefficients.

:Authors:
    Mark Driver <mdd31>

Attributes
----------

RESOURCES_PATH : pathlib.Path
    Path to resources directory where information is stored.

SINGLE_SSIP_SOLUTE_FILE : str
    POSIX path to the solute file containing solutes with a single solute SSIP,
    for use in SSI and FGIP calculation. Solutes have an SSIP value between
    -20.0 and +10.0 (0.1 increment).

"""

import logging
import pathlib
import csv
import numpy as np

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


RESOURCES_PATH = pathlib.Path(__file__).parents[0] / "resources"

SINGLE_SSIP_SOLUTE_FILE = (RESOURCES_PATH / "solutefile" / "single_ssip_value_list.xml").resolve().as_posix()


def get_name_inchikey_mapping():
    """Get molecule name: InChIKey pairs.

    Returns
    -------
    dict
        DESCRIPTION.

    """
    inchikey_file = (RESOURCES_PATH / "solventinchikeys.csv").resolve().as_posix()
    with open(inchikey_file, 'r') as mapfile:
        reader = csv.reader(mapfile, delimiter='\t')
        reader.__next__()
        return {entry[0]: entry[1] for entry in reader}

def get_ssip_file_names():
    """Get SSIP filenames. This defaults to the polarised SSIP descriptions.

    Returns
    -------
    Generator
        Yields path entries for each SSIP filename

    """
    return get_polarised_ssip_filenames()

def get_polarised_ssip_filenames():
    """Get polarised SSIP filenames.

    Returns
    -------
    Generator
        Yields path entries for each SSIP filename

    """
    return RESOURCES_PATH.glob("polarisedssipfiles/*.xml")


def get_unpolarised_ssip_filenames():
    """Get unpolarised SSIP filenames.

    Returns
    -------
    Generator
        Yields path entries for each SSIP filename

    """
    return RESOURCES_PATH.glob("unpolarisedssipfiles/*.xml")
    

def get_ssip_file_dict():
    """Get dictionary of SSIP file posix paths. This defaults to the polarised SSIP descriptions.

    Returns
    -------
    dict
        InChIKey: filename posix path pairs

    """
    return {
        filename.name.replace("_ssip.xml", ""): filename.absolute().as_posix()
        for filename in get_ssip_file_names()
    }

def get_polarised_ssip_file_dict():
    """Get dictionary of polarised SSIP file posix paths.

    Returns
    -------
    dict
        InChIKey: filename posix path pairs

    """
    return {
        filename.name.replace("_ssip.xml", ""): filename.absolute().as_posix()
        for filename in get_polarised_ssip_filenames()
    }

def get_unpolarised_ssip_file_dict():
    """Get dictionary of unpolarised SSIP file posix paths for unpolarised files.

    Returns
    -------
    dict
        InChIKey: filename posix path pairs

    """
    return {
        filename.name.replace("_ssip.xml", ""): filename.absolute().as_posix()
        for filename in get_unpolarised_ssip_filenames()
    }


def get_polynomial_filenames():
    """Polynomial file posix paths for pure solvents.

    Returns
    -------
    list
        Polynomial file posix paths for pure solvents.

    """
    return get_polarised_polynomial_filenames()

def get_polarised_polynomial_filenames():
    """Polynomial file posix paths for polarised pure solvents.

    Returns
    -------
    list
        Polynomial file posix paths for pure solvents.

    """
    return [
        filename.absolute().as_posix()
        for filename in RESOURCES_PATH.glob("polarisedpolynomialfiles/*free_poly_fit_split.csv")
    ]

def get_unpolarised_polynomial_filenames():
    """Polynomial file posix paths for unpolarised pure solvents.

    Returns
    -------
    list
        Polynomial file posix paths for pure solvents.

    """
    return [
        filename.absolute().as_posix()
        for filename in RESOURCES_PATH.glob("unpolarisedpolynomialfiles/*free_poly_fit_split.csv")
    ]

def get_similarity_domain_information():
    """Similarity domain information required for calculation of similarity metric.

    Returns
    -------
    x_values_list : list of arrays
        List of points in each domain to calculate solvation energy at.
    normalisation_coefficients : list
        Normalisation coefficients for domains in x_values_list.

    """
    x_values_list = [
        np.array([(float(x) / 10.0) - 10.0 for x in range(51)]),
        np.array([(float(x) / 10.0) - 5.0 for x in range(31)]),
        np.array([(float(x) / 10.0) - 2.0 for x in range(21)]),
        np.array([(float(x) / 10.0) - 0.0 for x in range(11)]),
        np.array([(float(x) / 10.0) + 1.0 for x in range(21)]),
        np.array([(float(x) / 10.0) + 3.0 for x in range(21)]),
    ]

    normalisation_coefficients = [
        29.573840367308566,
        9.4352526310819851,
        2.1109705595898145,
        1.981619630257929,
        11.244516687700679,
        30.555524859353685,
    ]
    return x_values_list, normalisation_coefficients
