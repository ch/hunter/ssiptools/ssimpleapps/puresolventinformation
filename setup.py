#    puresolventinformation contains default information on the set of pure solvents for FGIP analysis.
#    Copyright (C) 2019  Mark D. Driver
#
#    puresolventinformation is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup

setup(name='puresolventinformation',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='Collection of information for FGIP and solvent similarity score calculation.',
      url='https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/puresolventinformation',
      author='Mark Driver',
      author_email='mdd31@cantab.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':['resources/*', 'resources/*/*']},
      install_requires=['numpy'],
      zip_safe=False)

