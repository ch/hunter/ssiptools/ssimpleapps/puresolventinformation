# Pure Solvent Information #

A python package containing the information of the pure solvents for use in FGIP creation and solvent similarity comparisons.

## How do I get set up? ##

To build and install this module from source please follow the below instructions.

### Clone the repository ###

*From bitbucket:*

    git clone https://bitbucket.org/mdd31/puresolventinformation.git
    

*From University of Cambridge gitlab:*

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/ssimpleapps/puresolventinformation.git

Change to the repository: 

    cd puresolventinformation


### Setting up the python environment ###

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda env create -f environment.yml

This creates an environment called 'pureinf' which can be loaded using:

	conda activate pureinf


### Using pip to install module ###

To install in your environment using pip run:

	pip install .

This installs it in your current python environment.


### Expected usage

This module contains information on solvents for use with the SSIMPLE approach.

Please see the documentation for more information.

### Documentation ###

Documentation can be rendered using sphinx.

	cd docs
	make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk
