.. puresolventinformation documentation master file, created by
   sphinx-quickstart on Mon Jan  6 16:30:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to puresolventinformation's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README.md
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
